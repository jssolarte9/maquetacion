import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppComponent } from './app.component';
import { Tarjeta } from './Components/Tarjeta/Tarjeta.component';

@NgModule({
  declarations: [
    AppComponent,
    Tarjeta
  ],
  imports: [
    BrowserModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
